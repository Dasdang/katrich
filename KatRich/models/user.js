﻿// Require Mongoose
var mongoose = require('mongoose');
var bcrypt = require('bcrypt');

// Define schema
var Schema = mongoose.Schema;

var userSchema = new Schema({
    name: { type: String, max: 100 },
    password: { type: String, required: true, max: 100 },
    role: { type: String, enum: ['Admin', 'Customer',] },

    gender: { type: String, enum: ['Male', 'Female',] },
    image: { type: String, max: 100, default: '/images/users/default.png' },

    email: { type: String, required: true, max: 100, unique: true, },
    phone: { type: String, max: 15 },
    address: { type: String, max: 200 },

    note: { type: String, max: 100 },
}, { collection: 'users', versionKey: false });


userSchema
.virtual('userDetail')
.get(function () {
    return '/auth/profile/' + this._id;
});

userSchema
.virtual('userEdit')
.get(function () {
    return '/auth/profile/edit/' + this._id;
});

userSchema
.methods
.copySignupFrom = function (obj, cb) {

    this.name = obj.name;
    this.password = obj.password;
    this.email = obj.email;
    this.gender = 'Male';
    this.role = 'Customer';
    this.address = '';
    this.phone = '';
    this.note = '';
    this.image = '/images/users/default.png';
    return this;
};

userSchema
.statics
.getAUser = function(id, cb) {
    return this.model('User').findById(id, cb);
};

//hashing a password before saving it to the database
userSchema.pre('save', function (next) {
    let newUser = this;
    bcrypt.hash(newUser.password, 12, function (err, hash) {
        if (err) {
            return next(err);
        }
        newUser.password = hash;
        next();
    })
});

//authenticate input against database
userSchema
.statics
.authenticate = function (email, password, callback) {
    this.model('User').findOne({ email: email })
        .exec(function (err, user) {
            if (err) {
                return callback(err);
            } else if (!user) {
                var err = new Error('User not found.');
                err.status = 401;
                return callback(err);
            }
            bcrypt.compare(password, user.password, function (err, result) {
                if (result === true) {
                    return callback(null, user);
                } else {
                    return callback();
                }
            })
        });
}

userSchema
.statics
.doesExisted = function (email, callback) {
    return this.model('User').findOne({ email: email }, callback);
}

userSchema
.statics
.checkPassword = function (pass1, pass2, callback){
    bcrypt.compare(pass1, pass2, function (err, result) {
        if (result === true) {
            return callback(null, true);
        } else {
            return callback(null, false);
        }
    })
}

userSchema
.statics
.getHashPassword = function (newpass, callback){
    return bcrypt.hash(newpass, 12, callback);
}

// Export model
module.exports = mongoose.model('User', userSchema);
