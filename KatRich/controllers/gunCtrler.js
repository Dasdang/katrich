﻿var gun = require('../models/gun');
var user = require('../models/user');


// Display 6 new guns for each category for Home
exports.listNewItems = function (req, res) {
    gun.find({ 'category': 'Rifle' }, 'name image price').sort({ imported: -1, price: -1 }).limit(6)
        .exec(function (err, rifles) {
            if (err) { return err; }

            gun.find({ 'category': 'Pistol' }, 'name image price').sort({ imported: -1, price: -1 }).limit(6)
                .exec(function (err, pistols) {
                    if (err) { return err; }

                    // load user
                    if (!req.session.userId) {
                        res.render('index', { title: 'Home', listOfRifles: rifles, listOfPistols: pistols });
                    }
                    else {
                        user.getAUser(req.session.userId, function (err, loggedUser) {
                            if (err) { return err; }
                            if (loggedUser == null) { // No results.
                                var err = new Error('User not found');
                                err.status = 404;
                                return next(err);
                            }
                            // Successful, so render.
                            res.render('index', { title: 'Home', loggedUser, listOfRifles: rifles, listOfPistols: pistols });
                        });
                    }
                });
        });
};

// Display all pistols for Pistol
exports.listPistols = function (req, res) {

    let activePage = 1; if (req.params.page) activePage = req.params.page;
    let minNumItems = 8;
    let orderField = 'name';
    let orderSort = 1;

    gun.list('Pistol', function (err, allGuns) {
        if (err) { return err; }
        maxPage = Math.ceil(allGuns.length / minNumItems);

        // main get
        gun.find({ 'category': 'Pistol' }).skip((activePage - 1) * minNumItems).limit(minNumItems)
            .exec(function (err, guns) {
                if (err) { return err; }
                let pages = Array.from(Array(maxPage), (x, index) => index + 1);

                // load user
                if (!req.session.userId) {
                    res.render('pistols', { title: 'Pistols', listOfGuns: guns, pages, activePage });
                }
                else {
                    user.getAUser(req.session.userId, function (err, loggedUser) {
                        if (err) { return err; }
                        if (loggedUser == null) { // No results.
                            var err = new Error('User not found');
                            err.status = 404;
                            return next(err);
                        }
                        // Successful, so render.
                        res.render('pistols', { title: 'Pistols', loggedUser, listOfGuns: guns, pages, activePage });
                    });
                }
            });
    });
};

// Display all rifles for Rifle
exports.listRifles = function (req, res) {
    
    let activePage = 1; if (req.params.page) activePage = req.params.page;
    let minNumItems = 8;
    let orderField = 'name';
    let orderSort = 1;

    gun.list('Rifle', function (err, allGuns) {
        if (err) { return err; }
        maxPage = Math.ceil(allGuns.length / minNumItems);
        
        // main get
        gun.find({ 'category': 'Rifle' }).skip((activePage - 1) * minNumItems).limit(minNumItems)
            .exec(function (err, guns) {
                if (err) { return err; }
                let pages = Array.from(Array(maxPage), (x, index) => index + 1);

                // load user
                if (!req.session.userId) {
                    res.render('rifles', { title: 'Rifles', listOfGuns: guns, pages, activePage });
                }
                else {
                    user.getAUser(req.session.userId, function (err, loggedUser) {
                        if (err) { return err; }
                        if (loggedUser == null) { // No results.
                            var err = new Error('User not found');
                            err.status = 404;
                            return next(err);
                        }
                        
                        // Successful, so render.
                        res.render('rifles', { title: 'Rifles', loggedUser, listOfGuns: guns, pages, activePage });
                    });
                }
            });
    });
};

// Display detail page for a specific gun
exports.gunDetail = function (req, res, next) {
    gun.findById(req.params.id)
        .exec(function (err, specificGun) {
            if (err) { return err; }
            if (specificGun == null) { // No results.
                var err = new Error('Gun not found');
                err.status = 404;
                return next(err);
            }

            let cate = specificGun.category;
            if (cate === 'Rifle') cate = 'Pistol';
            else cate = 'Rifle';

            gun.find({ 'category': cate }, 'name image price').sort({ imported: -1, price: -1 }).skip(5).limit(4)
                .exec(function (err, guns) {
                    if (err) { return err; }

                    // load user
                    if (!req.session.userId) {
                        res.render('single', { title: 'Detail', gun: specificGun, listOfGuns: guns });
                    }
                    else {
                        user.getAUser(req.session.userId, function (err, loggedUser) {
                            if (err) { return err; }
                            if (loggedUser == null) { // No results.
                                var err = new Error('User not found');
                                err.status = 404;
                                return next(err);
                            }
                            // Successful, so render.
                            res.render('single', { title: 'Detail', loggedUser, gun: specificGun, listOfGuns: guns });
                        });
                    }
                });
        });
};

exports.search = function (req, res, next) {
    let tail = '?search=' + req.query.search;

    let activePage = 1; if (req.params.page) activePage = req.params.page;
    let minNumItems = 8;
    let orderField = 'name';
    let orderSort = 1;

    gun.find({ 'name': { $regex: req.query.search } }, function (err, allGuns) {
        if (err) { return err; }
        maxPage = Math.ceil(allGuns.length / minNumItems);

        // main get
        gun.find({ 'name': { $regex: req.query.search } }).skip((activePage - 1) * minNumItems).limit(minNumItems)
            .exec(function (err, guns) {
                if (err) { return err; }
                let pages = Array.from(Array(maxPage), (x, index) => index + 1);

                // load user
                if (!req.session.userId) {
                    res.render('rifles', { title: 'Rifles', listOfGuns: guns, pages, activePage, tail });
                }
                else {
                    user.getAUser(req.session.userId, function (err, loggedUser) {
                        if (err) { return err; }
                        if (loggedUser == null) { // No results.
                            var err = new Error('User not found');
                            err.status = 404;
                            return next(err);
                        }
                        // Successful, so render.
                        res.render('rifles', { title: 'Rifles', loggedUser, listOfGuns: guns, pages, activePage, tail });
                    });
                }
            });
        });
};
