﻿'use strict';
var express = require('express');
var router = express.Router();

// Require our controllers.
var gunCtrler = require('../controllers/gunCtrler');
var userCtrler = require('../controllers/userCtrler');


/// ROUTES ///
// GET catalog home page.
router.get('/', gunCtrler.listNewItems);

module.exports = router;
