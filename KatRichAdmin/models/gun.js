﻿// Require Mongoose
var mongoose = require('mongoose');

// Date handling
var moment = require('moment');

// Define schema
var Schema = mongoose.Schema;

var gunSchema = new Schema({
    name: { type: String, required: true, max: 100 },
    category: { type: String/*, enum: ['Pistol', 'Rifle',] */ },
    image: { type: String, default: '/images/guns/template.png', max: 100 },
    price: { type: Number, default: 0, min: 0 },
    description: { type: String, max: 500 },
    imported: { type: Date, default: Date.now() },
}, { collection: 'guns', versionKey: false });

gunSchema
.virtual('imported_yyyy_mm_dd')
.get(function () {
    return moment(this.imported).format('YYYY-MM-DD');
});

gunSchema
.virtual('gunDetail')
.get(function () {
    return '/product/productDetail/' + this._id;
});

gunSchema
.virtual('gunEdit')
.get(function () {
    return '/product/productEdit/' + this._id;
});

gunSchema
.virtual('gunDelete')
.get(function () {
    return '/product/productDelete/' + this._id;
});

gunSchema
.statics
.getHotGuns = function (number, cb) {
    return this.model('Gun').find({}, cb)
        .sort({ imported: 1, price: -1 })
        .limit(number);
};

gunSchema
.statics
.getAllGuns = function (cb) {
    return this.model('Gun').find({}, cb)
        .sort({ name: 1 });
};

gunSchema
.statics
.getAGun = function (id, cb) {
    return this.model('Gun').findById(id, cb);
};

gunSchema
.methods
.gunCopyFrom = function (obj, cb) {
    this.name = obj.name;
    this.category = obj.category;
    this.price = parseFloat(obj.price);
    if (null === this.price) this.price = 0;
    this.description = obj.description;
    this.image = obj.image;
    if (!this.image || this.image === '') this.image = '/images/guns/template.png';

    return this;
};

// Export model
module.exports = mongoose.model('Gun', gunSchema);