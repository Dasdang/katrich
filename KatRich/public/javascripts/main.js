function editText() {
    $('#gender').attr('disabled', false);
    $('#name').attr('readonly', false);
    $('#phone').attr('readonly', false);
    $('#address').attr('readonly', false);
    $('#note').attr('readonly', false);
    $('#acceptBtn').attr('disabled', false);
    $('#cancelBtn').attr('disabled', false);
    $('#acceptBtn').css('visibility', 'visible');
    $('#cancelBtn').css('visibility', 'visible');
    $('#imgBrowse').css('visibility', 'visible');
    $('#nameInput').css('visibility', 'visible');
    $('#nameDisplay').css('visibility', 'hidden');
    $('#collapsibleBtn').css('visibility', 'hidden');
}
document.getElementById("editBtn").onclick = editText;

function readURL(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
            $('#reviewImg').attr('src', e.target.result);
        }

        reader.readAsDataURL(input.files[0]);
    }
}
$('#imgBrowse').change(function () {
    readURL(this);
});

document.getElementById("cancelBtn").onclick = function cancelEdit() {
    location.href = location;
}

function cancelChange() {

    if ($('#passwordChange').css('display') === "block") {
        $('#infoChange').css('display', 'block');
        $('#passwordChange').css('display', 'none');
    } else {
        $('#infoChange').css('display', 'none');
        $('#passwordChange').css('display', 'block');
    }
}
document.getElementById("collapsibleBtn").onclick = function expand() {

    if ($('#passwordChange').css('display') === "block") {
        $('#infoChange').css('display', 'block');
        $('#passwordChange').css('display', 'none'); 
    } else {
        $('#infoChange').css('display', 'none');
        $('#passwordChange').css('display', 'block');
    }
}

$('#passform').submit(function (e) {
    $.getJSON('/auth/changePass?oldpass=' + $('#oldpass-change').val() + '&newpass=' + $('#newpass-change').val(), function (data) {
        if (data) { location.href = location; }
        else { $('#error-change').html('Your old password is incorrect!'); }
    })
    return false;
});
$('#repass-change').keyup(function () {
    if ($('#repass-change').val() !== $('#newpass-change').val()) {
        $('#error-change').html('Confirm New Password does not match!');
        $('#accept-change-btn').attr('disabled', true);
        $('#accept-change-btn').css('text-decoration', 'line-through');
    }
    else {
        $('#error-change').html('');
        $('#accept-change-btn').attr('disabled', false);
        $('#accept-change-btn').css('text-decoration', 'none');
    }
});