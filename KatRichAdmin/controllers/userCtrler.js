﻿var user = require('../models/user');
var cloud = require('../config/cloudinary');


// Display all users for User List
exports.listAllUsers = function (req, res, next) {
    user.find({}).sort({ name: 1 })
        .exec(function (err, users) {
            if (err) { return err; }

            // load user
            if (!req.session.userId) {
                res.redirect('/');
            }
            else {
                user.getAUser(req.session.userId, function (err, loggedUser) {
                    if (err) { return err; }
                    if (loggedUser == null) { // No results.
                        var err = new Error('User not found');
                        err.status = 404;
                        return next(err);
                    }
                    // Successful, so render.
                    res.render('userList', { title: 'User List', loggedUser, listOfUsers: users });
                });
            }
        });
};

// Display a specific user for User Edit
exports.userEdit = function (req, res, next) {
    user.findById(req.params.id)
        .exec(function (err, specificUser) {
            if (err) { return err; }
            if (specificUser == null) { // No results.
                var err = new Error('User not found');
                err.status = 404;
                return next(err);
            }

            // load user
            if (!req.session.userId) {
                res.redirect('/');
            }
            else {
                user.getAUser(req.session.userId, function (err, loggedUser) {
                    if (err) { return err; }
                    if (loggedUser == null) { // No results.
                        var err = new Error('User not found');
                        err.status = 404;
                        return next(err);
                    }
                    // Successful, so render.
                    res.render('userEdit', { title: 'User Edit', loggedUser, user: specificUser });
                });
            }
        });
};

// Display a specific user for User Detail
exports.userDetail = function (req, res, next) {
    user.getAUser(req.params.id, function (err, specificUser) {
        if (err) { return err; }
        if (specificUser == null) { // No results.
            var err = new Error('User not found');
            err.status = 404;
            return next(err);
        }

        // load user
        if (!req.session.userId) {
            res.redirect('/');
        }
        else {
            user.getAUser(req.session.userId, function (err, loggedUser) {
                if (err) { return err; }
                if (loggedUser == null) { // No results.
                    var err = new Error('User not found');
                    err.status = 404;
                    return next(err);
                }
                // Successful, so render.
                res.render('userDetail', { title: 'User Detail', loggedUser, user: specificUser });
            });
        }
    });
};   

// Get submit from Post, do and back to User List
exports.userEditSubmit = async (req, res, next) => {
   
    if (req.body._id !== '') {
        console.log('edit');
        const tempUser = {
            _id: req.body._id,
            name: req.body.name,
            gender: req.body.gender,
            role: req.body.role,
            email: req.body.email,
            address: req.body.address,
            phone: req.body.phone,
            note: req.body.note,
        }

        if (req.files[0]) {
            const imageDetails = {
                imageName: req.files[0].originalname,
                cloudImage: req.files[0].path,
                imageId: ''
            }
            console.log(imageDetails);
            await cloud.uploads(imageDetails.cloudImage).then((result) => {
                const imageDetails = {
                    cloudImage: result.url,
                    imageId: result.id
                }

                tempUser.image = imageDetails.cloudImage;
                console.log(imageDetails.imageId);
            });
        }

        console.log(tempUser);
        
        user.findByIdAndUpdate(
            tempUser._id,
            tempUser,
            { new: true },
            (err) => {
                var err = new Error('Update Error');
                err.status = 500;
                if (err) return next(err);
            });
    }
    else {
        console.log('add');

        const tempUser = new user();
        tempUser.copyFrom(req.body);

        if (req.files[0]) {
            const imageDetails = {
                imageName: req.files[0].originalname,
                cloudImage: req.files[0].path,
                imageId: ''
            }
            await cloud.uploads(imageDetails.cloudImage).then((result) => {
                const imageDetails = {
                    imageName: req.files[0].originalname,
                    cloudImage: result.url,
                    imageId: result.id
                }
                
                tempUser.image = imageDetails.cloudImage;
                console.log(imageDetails.imageId);
            });
        }
        
        console.log(tempUser);

        tempUser.save(err => {
            var err = new Error('Create Error');
            err.status = 500;
            if (err) return next(err);
        });
    }
   
    // Successful, so render.
    res.redirect('/user');
};

// Get order, do and back to User List
exports.userDelete = function (req, res, next) {
    user.findByIdAndRemove(req.params.id, (err) => {
        var err = new Error('Delete Error');
        err.status = 500;
        if (err) return next(err);
    });
   
    // Successful, so render.
    res.redirect('/user');
};

// Display a blank user for User Add
exports.userAdminAdd = function (req, res, next) {
    let tempUser = {};

    // load user
    if (!req.session.userId) {
        res.redirect('/');
    }
    else {
        user.getAUser(req.session.userId, function (err, loggedUser) {
            if (err) { return err; }
            if (loggedUser == null) { // No results.
                var err = new Error('User not found');
                err.status = 404;
                return next(err);
            }
            // Successful, so render.
            res.render('userEdit', { title: 'User Add', loggedUser, user: tempUser });
        });
    }
};


exports.authenticate = function (req, res, next) {
    console.log(req.body);

    if (req.body.email && req.body.password) {
        user.authenticate(req.body.email, req.body.password, function (error, tempUser) {
            if (error || !tempUser) {
                var err = new Error('Wrong email or password.');
                err.status = 401;
                if (err) return next(err);
            } else {
                req.session.userId = tempUser._id;
                res.redirect('/dashboard/');
                res.end();
            }
        });
    } else {
        res.redirect('/');
        var err = new Error('All fields required.');
        err.status = 400;
        if (err) return next(err);
    }

    // 404
};

exports.userProfile = function (req, res, next) {
    if (!req.session.userId) {
        res.redirect('/');
    }
    else {
        user.getAUser(req.session.userId, function (err, loggedUser) {
            if (err) { return err; }
            if (loggedUser == null) { // No results.
                var err = new Error('User not found');
                err.status = 404;
                return next(err);
            }
            // Successful, so render.
            res.render('userDetail', { title: 'User Detail', loggedUser, user: loggedUser });
        });
    }
};

exports.logout = function (req, res, next) {
    if (req.session) {
        // delete session object
        req.session.destroy(function (err) {
            if (err) {
                return next(err);
            } else {
                return res.redirect('/');
            }
        });
    }
};

/*
//POST route for updating data
router.post('/', function (req, res, next) {
    // confirm that user typed same password twice
    if (req.body.password !== req.body.passwordConf) {
        var err = new Error('Passwords do not match.');
        err.status = 400;
        res.send("passwords dont match");
        return next(err);
    }

    if (req.body.email &&
        req.body.username &&
        req.body.password &&
        req.body.passwordConf) {

        var userData = {
            email: req.body.email,
            username: req.body.username,
            password: req.body.password,
        }

        User.create(userData, function (error, user) {
            if (error) {
                return next(error);
            } else {
                req.session.userId = user._id;
                return res.redirect('/profile');
            }
        });

    } else if (req.body.logemail && req.body.logpassword) {
        User.authenticate(req.body.logemail, req.body.logpassword, function (error, user) {
            if (error || !user) {
                var err = new Error('Wrong email or password.');
                err.status = 401;
                return next(err);
            } else {
                req.session.userId = user._id;
                return res.redirect('/profile');
            }
        });
    } else {
        var err = new Error('All fields required.');
        err.status = 400;
        return next(err);
    }
})

// GET route after registering
router.get('/profile', function (req, res, next) {
    User.findById(req.session.userId)
        .exec(function (error, user) {
            if (error) {
                return next(error);
            } else {
                if (user === null) {
                    var err = new Error('Not authorized! Go back!');
                    err.status = 400;
                    return next(err);
                } else {
                    return res.send('<h1>Name: </h1>' + user.username + '<h2>Mail: </h2>' + user.email + '<br><a type="button" href="/logout">Logout</a>')
                }
            }
        });
});
*/