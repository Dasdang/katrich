﻿var express = require('express');
var router = express.Router();


// Require our controllers.
var gunCtrler = require('../controllers/gunCtrler');
var userCtrler = require('../controllers/userCtrler');


/// ROUTES ///
router.get('/rifles', gunCtrler.listRifles);

router.get('/rifles/:page', gunCtrler.listRifles);

router.get('/pistols', gunCtrler.listPistols);

router.get('/pistols/:page', gunCtrler.listPistols);

router.get('/gun/:id', gunCtrler.gunDetail);

router.get('/search', gunCtrler.search);

router.get('/search/:page', gunCtrler.search);

module.exports = router;
