﻿var express = require('express');
var multer = require('multer');
var router = express.Router();


// Require our controllers.
var userCtrler = require('../controllers/userCtrler');
var upload = multer({ dest: 'uploads/' });

/// ROUTES ///
router.post('/authenticate/', userCtrler.authenticate);

router.post('/signup/', userCtrler.signup);

router.get('/profile/:id', userCtrler.userDetail);

router.get('/logout/', userCtrler.logout);

router.get('/checkExisting/', userCtrler.checkExisting);

router.get('/login/', userCtrler.login);

router.post('/update/', upload.any(), userCtrler.updateSubmit);

router.get('/changePass/', userCtrler.changePass);

module.exports = router;
