﻿// Require Mongoose
var mongoose = require('mongoose');

// Define schema
var Schema = mongoose.Schema;

var categorySchema = new Schema({
    name: { type: String, required: true, max: 100 },
}, { collection: 'categories', versionKey: false });

categorySchema
.statics
.getAllCategories = function (cb) {
    return this.model('Category').find({}, cb)
        .sort({ name: 1 });
};

categorySchema
.methods
.copyFrom = function (obj, cb) {
    this.name = obj.name;

    return this;
};

categorySchema
.virtual('cateDelete')
.get(function () {
    return '/product/cateDelete/' + this._id;
});

// Export model
module.exports = mongoose.model('Category', categorySchema);