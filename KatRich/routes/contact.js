﻿var express = require('express');
var router = express.Router();


// Require our controllers.
var shopCtrler = require('../controllers/shopCtrler');


/// ROUTES ///
router.get('/', shopCtrler.contact);


module.exports = router;
