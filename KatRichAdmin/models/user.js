﻿// Require Mongoose
var mongoose = require('mongoose');
var bcrypt = require('bcrypt');

// Define schema
var Schema = mongoose.Schema;

var userSchema = new Schema({
    name: { type: String, max: 100 },
    password: { type: String, required: true, max: 100 },
    role: { type: String, enum: ['Admin', 'Customer',] },

    gender: { type: String, enum: ['Male', 'Female',] },
    image: { type: String, max: 100, default: '/images/guns/template.png' },
    
    email: { type: String, required: true, max: 100, unique: true, },
    phone: { type: String, max: 15 },
    address: { type: String, max: 200 },

    note: { type: String, max: 100 },
}, { collection: 'users', versionKey: false });

userSchema
.virtual('userDetail')
.get(function () {
    return '/user/userDetail/' + this._id;
});

userSchema
.virtual('userEdit')
.get(function () {
    return '/user/userEdit/' + this._id;
});

userSchema
.virtual('userDelete')
.get(function () {
    return '/user/userDelete/' + this._id;
});

userSchema
.statics
.getAUser = function (id, cb) {
    return this.model('User').findById(id, cb);
};

userSchema
.statics
.getAllUsers = function (cb) {
    return this.model('User').find({}, cb)
        .sort({ name: 1 });
};

//hashing a password before saving it to the database
userSchema.pre('save', function (next) {
    let user = this;
    bcrypt.hash(user.password, 12, function (err, hash) {
        if (err) {
            return next(err);
        }
        user.password = hash;
        next();
    })
});

//authenticate input against database
userSchema
.statics
.checkEmail = function (email, callback) {
    this.model('User').findOne({ email: email })
        .exec(function (err, user) {
            if (err) {
                return callback(err)
            } else if (!user) {
                return true;
            }
            else {
                return false;
            }
        });
}


userSchema
.statics
.authenticate = function (email, password, callback) {
    this.model('User').findOne({ email: email })
        .exec(function (err, user) {
            if (err) {
                return callback(err)
            } else if (!user) {
                var err = new Error('User not found.');
                err.status = 401;
                return callback(err);
            }
            bcrypt.compare(password, user.password, function (err, result) {
                if (result === true) {
                    if (user.role === 'Admin')
                        return callback(null, user);
                    else 
                        return callback();
                } else {
                    return callback();
                }
            })
        });
}

userSchema
.methods
.copyFrom = function (obj, cb) {
    this.name = obj.name;
    this.password = obj.password;
    this.gender = obj.gender;
    this.role = obj.role;
    this.email = obj.email;
    this.address = obj.address;
    this.phone = obj.phone;
    this.note = obj.note;
    this.image = '/images/guns/template.png';

    return this;
};

userSchema
.methods
editingCopyFrom = function (obj, cb) {
    this.id = obj._id;
    this.name = obj.name;
    this.gender = obj.gender;
    this.role = obj.role;
    this.email = obj.email;
    this.address = obj.address;
    this.phone = obj.phone;
    this.note = obj.note;
    
    return this;
};

/* THIS IS FAIL
userSchema
.methods
uploadImage = async (img, cb) => {
    const imageDetails = {
        imageName: img.originalname,
        cloudImage: imgte.path,
        imageId: ''
    }
    await cloud.uploads(imageDetails.cloudImage).then((result) => {
        const imageDetails = {
            cloudImage: result.url,
            imageId: result.id
        }

        this.image = imageDetails.cloudImage;
    });
};*/

// Export model
module.exports = mongoose.model('User', userSchema);
