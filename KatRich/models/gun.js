﻿// Require Mongoose
var mongoose = require('mongoose');

// Date handling
var moment = require('moment');

// Define schema
var Schema = mongoose.Schema;

var gunSchema = new Schema({
    name: { type: String, required: true, max: 100 },
    category: { type: String, enum: ['Pistol', 'Rifle',] },
    image: { type: String, max: 100 },
    price: { type: Number, default: 0, min: 0 },
    description: { type: String, max: 500 },
    imported: { type: Date, default: Date.now() },
});

gunSchema
.virtual('imported_yyyy_mm_dd')
.get(function () {
    return moment(this.imported).format('YYYY-MM-DD');
});

gunSchema
.virtual('url')
.get(function () {
    return '/shop/gun/' + this._id;
});

// get gun type
gunSchema
.statics
.list = function (cate, cb) {
    return this.model('Gun')
        .find({ 'category': cate }, cb);
}

// Export model
module.exports = mongoose.model('Gun', gunSchema);