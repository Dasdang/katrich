﻿var express = require('express');
var multer = require('multer');
var router = express.Router();


// Require our controllers.
var gunCtrler = require('../controllers/gunCtrler');
var cateCtrler = require('../controllers/categoryCtrler');

var upload = multer({ dest: 'uploads/' });

router.get('/', gunCtrler.listAllItems);

router.get('/productDetail/:id', gunCtrler.gunAdminDetail);

router.get('/productEdit/:id', gunCtrler.gunAdminEdit);

router.post('/productEditSubmit/', upload.any(), gunCtrler.gunAdminEditSubmit);

router.get('/productAdd', gunCtrler.gunAdminAdd);

router.get('/productDelete/:id', gunCtrler.gunAdminDelete);

router.post('/categoryAddSubmit/', cateCtrler.cateAddSubmit);

router.get('/cateDelete/:id', cateCtrler.cateDelete);

module.exports = router;
