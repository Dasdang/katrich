﻿var user = require('../models/user');
var cloud = require('../config/cloudinary');


// Display a specific user for User Edit
exports.userEdit = function (req, res, next) {
    if (!req.session.userId) {
        res.redirect('/');
    }
    else {
        user.findById(req.params.id)
            .exec(function (err, loggedUser) {
                if (err) { return err; }
                if (loggedUser == null) { // No results.
                    var err = new Error('User not found');
                    err.status = 404;
                    return next(err);
                }
                // Successful, so render.
                res.render('userEdit', { title: 'User Edit', loggedUser, user: loggedUser });
            });
    }
};

// Display a specific user for User Detail
exports.userDetail = (req, res, next) => {
    if (!req.session.userId) {
        res.redirect('/');
    }
    else {
        user.getAUser(req.session.userId, function (err, loggedUser) {
            if (err) { return err; }
            if (loggedUser == null) { // No results.
                var err = new Error('User not found');
                err.status = 404;
                return next(err);
            }
            // Successful, so render.
            res.render('profile', { title: 'Profile', loggedUser, user: loggedUser });
        });
    }
    //const specificUser = await user.detail(req.params.id);
    //res.render('profile', { title: 'Profile', user: specificUser });
};   

// Get submit from post Sign Up
exports.signup = function (req, res, next) {   
    console.log('sign up');

    if (req.body.name && req.body.email
        && req.body.password && req.body.confirmPassword) {

        if (req.body.password === req.body.confirmPassword) {

            let tempUser = new user();
            tempUser.copySignupFrom(req.body);

            tempUser.save(function (err, newUser) {
                if (err) return next(err);

                req.session.userId = tempUser._id;
                res.redirect('/auth/profile/' + newUser._id);
                res.end();
            });
        }
        else {
            var err = new Error('Wrong Password Confirm');
            err.status = 400;
            if (err) return next(err);
        }
    } else {
        var err = new Error('All fields required.');
        err.status = 400;
        if (err) return next(err);
    }
    
};

// Sign In request
exports.authenticate = function (req, res, next) {
    console.log(req.body);

    if (req.body.email && req.body.password) {
        user.authenticate(req.body.email, req.body.password, function (error, tempUser) {
            if (err) { return err; }

            if (!tempUser) {
                returnedMessage = 'Wrong email or password.'
            } else {
                req.session.userId = tempUser._id;
                res.redirect('/');
                res.end();
            }
        });
    } else {
        var err = new Error('All fields required.');
        err.status = 400;
        if (err) return next(err);
    }

    // 404
};

exports.logout = function (req, res, next) {
    if (req.session) {
        // delete session object
        req.session.destroy(function (err) {
            if (err) {
                return next(err);
            } else {
                return res.redirect('/');
            }
        });
    }
};

exports.checkExisting = function (req, res, next) {
    user.doesExisted(req.query.email, function (error, existedUser) {
        if (existedUser) {
            console.log(true);
            res.json(true);
        } else {
            console.log(false);
            res.json(false);
        }
    });
};

exports.login = function (req, res, next) {
    console.log(req.query);
    if (req.query.email && req.query.password) {
        user.authenticate(req.query.email, req.query.password, function (error, tempUser) {
            if (error || !tempUser) {
                res.json(false);
            } else {
                req.session.userId = tempUser._id;
                res.json(true);
            }
        });
    } else {
        res.json(false);
    }
};

// Get submit from Post and update info
exports.updateSubmit = async (req, res, next) => {
    console.log('edit');
    const tempUser = {
        _id: req.session.userId,
        name: req.body.name,
        gender: req.body.gender,
        address: req.body.address,
        phone: req.body.phone,
        note: req.body.note,
    }

    console.log(req.body);
    console.log(req.files);

    if (req.files[0]) {
        const imageDetails = {
            imageName: req.files[0].originalname,
            cloudImage: req.files[0].path,
            imageId: ''
        }
        console.log(imageDetails);
        await cloud.uploads(imageDetails.cloudImage).then((result) => {
            const imageDetails = {
                cloudImage: result.url,
                imageId: result.id
            }

            tempUser.image = imageDetails.cloudImage;
            console.log(imageDetails.imageId);
        });
    }

    console.log(tempUser);

    user.findByIdAndUpdate(
        tempUser._id,
        tempUser,
        { new: true },
        (err) => {
            var err = new Error('Update Error');
            err.status = 500;
            if (err) return next(err);
        });

    // Successful, so render.
    res.redirect('/auth/profile/' + req.session.userId);
};

exports.changePass = function (req, res, next) {
    console.log(req.query);
    if (req.query.oldpass && req.query.newpass) {
        
        user.getAUser(req.session.userId, function (err, loggedUser) {
            if (err || !loggedUser) {
                res.json(false);
            } else {
                user.checkPassword(req.query.oldpass, loggedUser.password, function (err, result) {
                    if (err) { console.log(err); }
                    if (result) {
                        console.log(true);

                        user.getHashPassword(req.query.newpass, function (err, hashPass) {
                            if (hashPass) {
                                loggedUser.password = hashPass;

                                user.findByIdAndUpdate(
                                    loggedUser._id, loggedUser, { new: true },
                                    (err) => {
                                        if (err) res.json(false);
                                        res.json(true);
                                    });
                            }
                            else {
                                res.json(false);
                            }
                        });
                    } else {
                        res.json(false);
                    }
                });
            }
        });
    } else {
        res.json(false);
    }
};
