﻿var express = require('express');
var router = express.Router();


// Require our controllers.
var userCtrler = require('../controllers/userCtrler');

// GET dashboard home page.
router.get('/', function (req, res) {
    res.render('login', { title: 'Login' });
});

router.post('/authenticate/', userCtrler.authenticate);

// GET /logout
router.get('/logout', userCtrler.logout);

module.exports = router;
