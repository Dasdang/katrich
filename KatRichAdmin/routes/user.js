﻿var express = require('express');
var multer = require('multer');
var router = express.Router();


// Require our controllers.
var userCtrler = require('../controllers/userCtrler');
var upload = multer({ dest: 'uploads/' });

router.get('/', userCtrler.listAllUsers);

router.get('/userDetail/:id', userCtrler.userDetail);

router.get('/userEdit/:id', userCtrler.userEdit);

router.post('/userEditSubmit/', upload.any(), userCtrler.userEditSubmit);

router.get('/userDelete/:id', userCtrler.userDelete);

router.get('/userAdd', userCtrler.userAdminAdd);

router.get('/userProfile', userCtrler.userProfile);

module.exports = router;
