﻿var gun = require('../models/gun');
var user = require('../models/user');


exports.contact = function (req, res) {
    // load user
    if (!req.session.userId) {
        res.render('contact', { title: 'Contact' });
    }
    else {
        user.getAUser(req.session.userId, function (err, loggedUser) {
            if (err) { return err; }
            if (loggedUser == null) { // No results.
                var err = new Error('User not found');
                err.status = 404;
                return next(err);
            }
            // Successful, so render.
            res.render('contact', { title: 'Contact', loggedUser });
        });
    }
};

exports.about = function (req, res) {
    // load user
    if (!req.session.userId) {
        res.render('about', { title: 'About' });
    }
    else {
        user.getAUser(req.session.userId, function (err, loggedUser) {
            if (err) { return err; }
            if (loggedUser == null) { // No results.
                var err = new Error('User not found');
                err.status = 404;
                return next(err);
            }
            // Successful, so render.
            res.render('about', { title: 'About', loggedUser });
        });
    }
};


exports.cart = function (req, res) {
    // load user
    if (!req.session.userId) {
        res.render('cart', { title: 'Cart' });
    }
    else {
        user.getAUser(req.session.userId, function (err, loggedUser) {
            if (err) { return err; }
            if (loggedUser == null) { // No results.
                var err = new Error('User not found');
                err.status = 404;
                return next(err);
            }
            // Successful, so render.
            res.render('cart', { title: 'Cart', loggedUser });
        });
    }
};

exports.checkout = function (req, res) {
    // load user
    if (!req.session.userId) {
        res.render('checkout', { title: 'Checkout' });
    }
    else {
        user.getAUser(req.session.userId, function (err, loggedUser) {
            if (err) { return err; }
            if (loggedUser == null) { // No results.
                var err = new Error('User not found');
                err.status = 404;
                return next(err);
            }
            // Successful, so render.
            res.render('checkout', { title: 'Checkout', loggedUser });
        });
    }
};