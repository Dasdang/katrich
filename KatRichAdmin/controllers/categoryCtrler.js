﻿var cate = require('../models/category');


// Get submit from Post, do and back to Product List
exports.cateAddSubmit = function (req, res, next) {
    console.log('add');

    const tempCate = new cate();
    tempCate.copyFrom(req.body);
    console.log(tempCate);

    tempCate.save(err => {
        var err = new Error('Create Error');
        err.status = 500;
        if (err) return next(err);
    });

    // Successful, so render.
    res.redirect('/product/productAdd');
};

exports.cateDelete = function (req, res, next) {
    console.log('delete');

    console.log(req.params.id);

    cate.findByIdAndRemove(req.params.id, (err) => {
        var err = new Error('Delete Error');
        err.status = 500;
        if (err) return next(err);
    });

    // Successful, so render.
    res.redirect('/product/productAdd');
};