﻿var user = require('../models/user');
var gun = require('../models/gun');


// Display 4 hot guns for Dashboard
exports.listHotItems = function (req, res, next) {
    gun.getHotGuns(4, function (err, guns) {
        if (err) { return err; }

        // load user
        if (!req.session.userId) {
            res.redirect('/');
        }
        else {
            user.getAUser(req.session.userId, function (err, loggedUser) {
                if (err) { return err; }
                if (loggedUser == null) { // No results.
                    var err = new Error('User not found');
                    err.status = 404;
                    return next(err);
                }
                // Successful, so render.
                res.render('index', { title: 'Dashboard', loggedUser, listOfGuns: guns });
            });
        }
    }); 
};

exports.analytics = function (req, res, next) {
    // load user
    if (!req.session.userId) {
        res.redirect('/');
    }
    else {
        user.getAUser(req.session.userId, function (err, loggedUser) {
            if (err) { return err; }
            if (loggedUser == null) { // No results.
                var err = new Error('User not found');
                err.status = 404;
                return next(err);
            }
            // Successful, so render.
            res.render('analytics', { title: 'Analytics', loggedUser });
        });
    }
};

exports.delivery = function (req, res, next) {
    // load user
    if (!req.session.userId) {
        res.redirect('/');
    }
    else {
        user.getAUser(req.session.userId, function (err, loggedUser) {
            if (err) { return err; }
            if (loggedUser == null) { // No results.
                var err = new Error('User not found');
                err.status = 404;
                return next(err);
            }
            // Successful, so render.
            res.render('delivery', { title: 'Delivery', loggedUser });
        });
    }
};