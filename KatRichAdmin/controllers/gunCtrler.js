﻿var user = require('../models/user');
var gun = require('../models/gun');
var cate = require('../models/category');
var cloud = require('../config/cloudinary');


// Display all guns for Product List
exports.listAllItems = function (req, res, next) {
    gun.getAllGuns(function (err, guns) {
        if (err) { return err; }

        // load user
        if (!req.session.userId) {
            res.redirect('/');
        }
        else {
            user.getAUser(req.session.userId, function (err, loggedUser) {
                if (err) { return err; }
                if (loggedUser == null) { // No results.
                    var err = new Error('User not found');
                    err.status = 404;
                    return next(err);
                }
                // Successful, so render.
                res.render('productList', { title: 'Product List', loggedUser, listOfGuns: guns });
            });
        }
    });      
};

// Display a specific gun for Product Detail
exports.gunAdminDetail = function (req, res, next) {
    gun.getAGun(req.params.id, function (err, specificGun) {
        if (err) { return err; }
        if (specificGun == null) { // No results.
            var err = new Error('Gun not found');
            err.status = 404;
            return next(err);
        }
        // load user
        if (!req.session.userId) {
            res.redirect('/');
        }
        else {
            user.getAUser(req.session.userId, function (err, loggedUser) {
                if (err) { return err; }
                if (loggedUser == null) { // No results.
                    var err = new Error('User not found');
                    err.status = 404;
                    return next(err);
                }
                // Successful, so render.
                res.render('productDetail', { title: 'Product Detail', loggedUser, gun: specificGun });
            });
        }
    });
};      

// Display a specific gun for Product Edit
exports.gunAdminEdit = function (req, res, next) {
    gun.getAGun(req.params.id, function (err, specificGun) {
        if (err) { return err; }
        if (specificGun == null) { // No results.
            var err = new Error('Gun not found');
            err.status = 404;
            return next(err);
        }

        cate.getAllCategories(function (err, cates) {
            // load user
            if (!req.session.userId) {
                res.redirect('/');
            }
            else {
                user.getAUser(req.session.userId, function (err, loggedUser) {
                    if (err) { return err; }
                    if (loggedUser == null) { // No results.
                        var err = new Error('User not found');
                        err.status = 404;
                        return next(err);
                    }
                    // Successful, so render.
                    res.render('productEdit', { title: 'Product Edit', loggedUser, gun: specificGun, categories: cates });
                });
            }
        });
    });
};

// Get submit from Post, do and back to Product List
exports.gunAdminEditSubmit = async (req, res, next) => {
    
    if (req.body._id !== '') {
        console.log('edit');
        const tempGun = {
            _id: req.body._id,
            name: req.body.name,
            category: req.body.category,
            price: req.body.price,
            description: req.body.description,
            imported: req.body.imported,
        }

        if (req.files[0]) {
            const imageDetails = {
                imageName: req.files[0].originalname,
                cloudImage: req.files[0].path,
                imageId: ''
            }
            console.log(imageDetails);
            await cloud.uploads(imageDetails.cloudImage).then((result) => {
                const imageDetails = {
                    cloudImage: result.url,
                    imageId: result.id
                }

                tempGun.image = imageDetails.cloudImage;
                console.log(imageDetails.imageId);
            });
        }

        console.log(tempGun);

        gun.findByIdAndUpdate(
            tempGun._id,
            tempGun,
            { new: true },
            (err) => {
                var err = new Error('Update Error');
                err.status = 500;
                if (err) return next(err);
            });
    }
    else {
        console.log('add');

        const tempGun = new gun();
        tempGun.gunCopyFrom(req.body);

        if (req.files[0]) {
            const imageDetails = {
                imageName: req.files[0].originalname,
                cloudImage: req.files[0].path,
                imageId: ''
            }
            await cloud.uploads(imageDetails.cloudImage).then((result) => {
                const imageDetails = {
                    imageName: req.files[0].originalname,
                    cloudImage: result.url,
                    imageId: result.id
                }

                tempGun.image = imageDetails.cloudImage;
                console.log(imageDetails.imageId);
            });
        }

        console.log(tempGun);

        tempGun.save(err => {
            var err = new Error('Create Error');
            err.status = 500;
            if (err) return next(err);
        });
    }

    // Successful, so render.
    res.redirect('/product');
};

// Get order, do and back to Product List
exports.gunAdminDelete = function (req, res, next) {
    gun.findByIdAndRemove(req.params.id, (err) => {
        var err = new Error('Delete Error');
        err.status = 500;
        if (err) return next(err);
    });

    // Successful, so render.
    res.redirect('/product');
};

// Display a blank gun for Product Add
exports.gunAdminAdd = function (req, res, next) {
    cate.getAllCategories(function (err, cates) {
        if (err) return res.status(500).send(err);

        tempGun = {};

        // load user
        if (!req.session.userId) {
            res.redirect('/');
        }
        else {
            user.getAUser(req.session.userId, function (err, loggedUser) {
                if (err) { return err; }
                if (loggedUser == null) { // No results.
                    var err = new Error('User not found');
                    err.status = 404;
                    return next(err);
                }
                // Successful, so render.
                res.render('productEdit', { title: 'Product Add', loggedUser, gun: tempGun, categories: cates });
            });
        }
    });
};
