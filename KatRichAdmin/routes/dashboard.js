﻿var express = require('express');
var router = express.Router();


// Require our controllers.
var dashboardCtrler = require('../controllers/dashboardCtrler');

// GET dashboard home page.
router.get('/', dashboardCtrler.listHotItems);

router.get('/analytics', dashboardCtrler.analytics);

router.get('/delivery', dashboardCtrler.delivery);

module.exports = router;
