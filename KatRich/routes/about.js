﻿var express = require('express');
var router = express.Router();


// Require controllers.
var shopCtrler = require('../controllers/shopCtrler');


/// ROUTES ///
router.get('/', shopCtrler.about);


module.exports = router;
